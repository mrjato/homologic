/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;

import es.uvigo.ei.sing.homologic.util.Checks;

public final class TestUtils {
	private TestUtils() {}

	public static void assertOkStatus(Response response) {
		assertStatus(OK, response);
	}
	
	public static void assertBadRequestStatus(Response response) {
		assertStatus(BAD_REQUEST, response);
	}
	
	public static void assertNotFoundStatus(Response response) {
		assertStatus(NOT_FOUND, response);
	}
	
	private static void assertStatus(StatusType status, Response response) {
		assertStatus("Unexpected status code: " + response.getStatus(), status, response);
	}
	private static void assertStatus(String message, StatusType status, Response response) {
		assertEquals(message, status, response.getStatusInfo());
	}
	
	public static void assertValidUUID(String uuid) {
		assertValidUUID(null, uuid);
	}
	
	public static void assertValidUUID(String message, String uuid) {
		try {
			Checks.requireUUID(uuid);
		} catch (NullPointerException | IllegalArgumentException e) {
			fail(message);
		}
	}
}
