/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.services;

import static es.uvigo.ei.sing.homologic.TestUtils.assertBadRequestStatus;
import static es.uvigo.ei.sing.homologic.TestUtils.assertNotFoundStatus;
import static es.uvigo.ei.sing.homologic.TestUtils.assertOkStatus;
import static es.uvigo.ei.sing.homologic.TestUtils.assertValidUUID;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.util.DigestUtils;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;

import es.uvigo.ei.sing.homologic.persistence.daos.RegistrationDAO;
import es.uvigo.ei.sing.homologic.persistence.daos.UserDAO;
import es.uvigo.ei.sing.homologic.persistence.entities.Registration;
import es.uvigo.ei.sing.homologic.persistence.entities.User;
import es.uvigo.ei.sing.homologic.services.entities.ErrorMessage;
import es.uvigo.ei.sing.homologic.services.entities.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration("file:src/test/resources/META-INF/applicationContext.xml")
@TestExecutionListeners({ 
	DependencyInjectionTestExecutionListener.class,
	DirtiesContextTestExecutionListener.class,
	TransactionDbUnitTestExecutionListener.class
})
@DatabaseSetup("file:src/test/resources/META-INF/dataset.xml")
public class RegistrationServiceTest {
	@Inject
	private RegistrationService service;
	
	@Inject
	private RegistrationDAO regDAO;
	
	@Inject
	private UserDAO userDAO;
	
	private Registration newRegistration;
	private Registration registration;
	private User user;
	
	@Before
	public void setUp() {
		this.newRegistration = new Registration(
			"ana", "ana@email.com", DigestUtils.md5DigestAsHex("ana".getBytes())
		);
		this.registration = new Registration(
			"juan", "juan@email.com", DigestUtils.md5DigestAsHex("juan".getBytes())
		);
		this.user = this.registration.toUser();
	}

	@Test
	public void testRegister() {
		final Response response = service.register(newRegistration);

		assertOkStatus(response);
		
		final Object entity = response.getEntity();
		assertThat(entity, instanceOf(UUID.class));
		assertValidUUID(((UUID) entity).getUuid());
		
		assertEquals(2, regDAO.list().size());
	}

	@Test
	public void testRegisterAgain() {
		final Response response = service.register(registration);

		assertOkStatus(response);

		final Object entity = response.getEntity();
		assertThat(entity, instanceOf(UUID.class));
		assertValidUUID(((UUID) entity).getUuid());
		
		assertEquals(1, regDAO.list().size());
	}

	@Test
	@ExpectedDatabase(value = "file:src/test/resources/META-INF/dataset.xml", 
		table = "Registration"
	)
	public void testRegisterExistentEmail() {
		final Registration registration = new Registration(
			"paco", "any@email.com", DigestUtils.md5DigestAsHex("anypass".getBytes())
		);
		final Response response = service.register(registration);

		assertBadRequestStatus(response);
		
		final Object entity = response.getEntity();
		assertThat(entity, instanceOf(ErrorMessage.class));
		assertEquals("Login already exists", ((ErrorMessage) entity).getMessage());
	}

	@Test
	@ExpectedDatabase(value = "file:src/test/resources/META-INF/dataset.xml", 
		table = "Registration"
	)
	public void testRegisterExistentLogin() {
		final Registration registration = new Registration(
			"any", "paco@email.com", DigestUtils.md5DigestAsHex("anypass".getBytes())
		);
		final Response response = service.register(registration);

		assertBadRequestStatus(response);
		
		final Object entity = response.getEntity();
		assertThat(entity, instanceOf(ErrorMessage.class));
		assertEquals("Email already exists", ((ErrorMessage) entity).getMessage());
	}

	@Test
	public void testConfirm() {
		final String uuid = "12345678-abcd-1234-cdef-0123456789ab";
		
		final Response response = service.confirm(new UUID(uuid));
		
		assertOkStatus(response);
		
		final User newUser = (User) response.getEntity();
		assertNotNull("Returned user is null", newUser);
		assertEquals("User doesn't corresponds to registration", this.user, newUser);

		assertEquals(userDAO.get(newUser.getLogin()), newUser);
		assertNull(regDAO.get(uuid));
	}

	@Test
	public void testConfirmNotExisting() {
		final Response response = service.confirm(
			new UUID("aaaaaaaa-bbbb-cccc-dddd-0123456789ab")
		);
		
		assertNotFoundStatus(response);
	}
}
