/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.persistence.daos;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public abstract class DAO<K, T> {
	@PersistenceContext
	protected EntityManager em;
	
	@SuppressWarnings("unchecked")
	protected Class<T> getEntityType() {
		return (Class<T>) ((ParameterizedType) getClass()
			.getGenericSuperclass())
	        .getActualTypeArguments()[1];
	}
	
	public T get(K key) {
		return this.em.find(this.getEntityType(), key);
	}
	
	public T persist(T entity) {
		this.em.persist(entity);
		
		return entity;
	}
	
	public T update(T entity) {
		return this.em.merge(entity);
	}
	
	public List<T> list() {
		final CriteriaQuery<T> query = createCBQuery();
		
		return em.createQuery(
			query.select(query.from(getEntityType()))
		).getResultList();
	}
	
	public void removeByKey(K key) {
		this.em.remove(get(key));
		this.em.flush();
	}
	
	public void remove(T entity) {
		this.em.remove(entity);
		this.em.flush();
	}
	
	protected <F> List<T> listBy(String fieldName, F value) {
		return createFieldQuery(fieldName, value)
			.getResultList();
	}
	
	protected <F> T getBy(String fieldName, F value) {
		try {
			return createFieldQuery(fieldName, value)
				.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	protected <F> TypedQuery<T> createFieldQuery(String fieldName, F value) {
		final CriteriaQuery<T> query = createCBQuery();
		final Root<T> root = query.from(getEntityType());
		
		return em.createQuery(
			query.select(root)
				.where(cb().equal(root.get(fieldName), value))
		);
	}
	
	protected CriteriaQuery<T> createCBQuery() {
		return cb().createQuery(this.getEntityType());
	}
	
	protected CriteriaBuilder cb() {
		return em.getCriteriaBuilder();
	}
}
