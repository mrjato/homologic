/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.persistence.entities;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private RoleId roleId;
	
	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	@JoinColumn(name = "login", referencedColumnName = "login")
	@MapsId("login")
	private User user;
	
	Role() {
	}
	
	public Role(User user, RoleType role) {
		this.user = requireNonNull(user);
		this.roleId = new RoleId(user.getLogin(), requireNonNull(role));
	}
	
	public User getUser() {
		return this.user;
	}
	
	public RoleType getRole() {
		return this.roleId.getRole();
	}
}
