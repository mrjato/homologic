/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.services;

import static es.uvigo.ei.sing.homologic.services.ServiceUtils.createBadRequestResponse;
import static es.uvigo.ei.sing.homologic.services.ServiceUtils.createInternalServerErrorResponse;
import static es.uvigo.ei.sing.homologic.services.ServiceUtils.doIfPrivileged;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.sun.jersey.multipart.FormDataParam;

import es.uvigo.ei.sing.homologic.controllers.GenomeController;
import es.uvigo.ei.sing.homologic.fasta.FastaParseException;
import es.uvigo.ei.sing.homologic.persistence.entities.TestGene;
import es.uvigo.ei.sing.homologic.persistence.entities.TestGenome;
import es.uvigo.ei.sing.homologic.persistence.file.StorageException;
import es.uvigo.ei.sing.homologic.services.entities.Gene;
import es.uvigo.ei.sing.homologic.services.entities.GeneNames;
import es.uvigo.ei.sing.homologic.services.entities.GenomeCreationMetadata;
import es.uvigo.ei.sing.homologic.services.entities.GenomeMetadata;
import es.uvigo.ei.sing.homologic.services.entities.Message;
import es.uvigo.ei.sing.homologic.services.entities.UUID;

@Path("genome")
@Service
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class TestGenomeService {
	private final static Logger LOG = LoggerFactory.getLogger(TestGenomeService.class);
	
	@Inject
	private GenomeController genomeController;
	
	@Context
	private SecurityContext security;
	
	/**
	 * <p>
	 * Uploads a new test genome file. The file will be just uploaded and
	 * stored as a raw file, and no data will be stored in the database.
	 * </p>
	 * <p>
	 * When a test genome file is correctly uploaded, a UUID will be returned
	 * that should be used in the {@code #store(GenomeMetadata)} method to
	 * create a new test genome for the corresponding user.
	 * </p>
	 * <p>
	 * <strong>Important: </strong>the content type of the request must be
	 * "multipart/form-data".
	 * </p>
	 * 
	 * 
	 * @param is The content of the request.
	 * @return The generated UUID with a 200 OK HTTP status if registration was
	 * successful or an error message with a 500 Internal Server Error HTTP
	 * status if an error occurs.
	 */
	@POST
	@Path("/file")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response upload(
		@FormDataParam("genome") InputStream is
	) {
		try {
			final String user = ServiceUtils.getUserName(security);
			final String uuid = genomeController.storeTest(user, is);
			
			return Response.ok(
				new UUID(uuid)
			).build();
		} catch (Exception e) {
			LOG.error("Error uploading test genome", e);
			
			return createInternalServerErrorResponse(
				"File could not be stored"
			);
		}
	}
	
	/**
	 * Creates a new test genome for the invoking user. The user must provide
	 * a valid UUID for a genome file previously uploaded.
	 * 
	 * @param genomeMetadata The name of the test genome and the UUID of its
	 * file.
	 * @return The metadata of the new genome with a 200 OK HTTP status if
	 * registration was successful, an error message with a 400 Bad Request
	 * HTTP status if the genome is not valid or an error message with a 500
	 * Internal Server Error HTTP status if an error occurs.
	 */
	@POST
	public Response store(
		GenomeCreationMetadata genomeMetadata
	) {
		try {
			final TestGenome genome = genomeController.createTest(
				ServiceUtils.getUserName(security), 
				genomeMetadata.getName(),
				genomeMetadata.getUuid()
			);
			
			return Response.ok(
				new GenomeMetadata(genome)
			).build();
		} catch (StorageException se) {
			return createInternalServerErrorResponse(
				"Error storing file: " + se.getMessage()
			);
		} catch (IOException | FastaParseException e) {
			LOG.error("Error parsing fasta file", e);
			
			return createBadRequestResponse(
				"Error parsing fasta file: " + e.getMessage()
			);
		} catch (Exception e) {
			LOG.error("Error storing genome", e);
			
			return createInternalServerErrorResponse(
				"Error storing genome: " + e.getMessage()
			);
		}
	}
	
	/**
	 * Returns the basic data of the test genomes that the user has in the
	 * system. The data is composed of the id, name and number of genes of the
	 * genome.
	 * 
	 * @return The basic data of the test genomes that the user has in the
	 * system with a 200 OK HTTP status if registration was successful or an
	 * error message with a 500 Internal Server Error HTTP status if an error
	 * occurs.
	 */
	@GET
	@Consumes(MediaType.WILDCARD)
	public Response list() {
		try {
			final String user = ServiceUtils.getUserName(security);
			
			final List<TestGenome> userGenomes = 
				genomeController.listTestFor(user);
			
			final List<GenomeMetadata> genomes = userGenomes.stream()
				.map(GenomeMetadata::new)
			.collect(toList());
			
			return Response.ok(genomes).build();
		} catch (Exception e) {
			LOG.error("Error retrieving user genome list", e);
			
			return ServiceUtils.createErrorResponse(
				INTERNAL_SERVER_ERROR, "Error retrieving genome list"
			);
		}
	}
	
	/**
	 * Deletes a test genome from the system. 
	 * 
	 * @return A confirmation message with a 200 OK HTTP status if deletion was
	 * successful, an error message with a 400 Bad Request HTTP status if the
	 * user does not have a test genome with the provided id or an error
	 * message with a 500 Internal Server Error HTTP status if an error occurs.
	 */
	@DELETE
	@Path("/{genomeId}")
	@Consumes(MediaType.WILDCARD)
	public Response delete(
		@PathParam("genomeId") int genomeId
	) {
		final TestGenome test = genomeController.getTest(genomeId);
		
		if (test == null) {
			return createBadRequestResponse("No genome found.");
		} else {
			final String login = test.getUser().getLogin();
			
			return doIfPrivileged(
				this.security,
				login,
				() -> {
					genomeController.deleteTest(login, genomeId);

					return Response.ok(
						new Message("Test genome " + genomeId + " deleted")
					).build();
				},
				e -> {
					LOG.error("Error deleting test genome " + genomeId, e);
					
					return createInternalServerErrorResponse(
						"Error deleting test genome: " + e.getMessage()
					);
				},
				() -> {
					LOG.warn(String.format(
						"Illegal access request of user %s to genome %d",
						login, genomeId
					));
					
					return createBadRequestResponse("No test genome found.");
				}
			);
		}
	}
	
	/**
	 * Returns a list with the name of the genes of a test genome.
	 * 
	 * @param genomeId Id of the test genome whose genes will be returned.
	 * @return A list with the name of the genes of the test genome identified
	 * by {@code genomeId} with a 200 OK HTTP status if the request was
	 * successful, an error message with a 400 Bad Request HTTP status if the
	 * user does not have a genome with the provided id, or an error message
	 * with a 500 Internal Server Error HTTP status if an error occurs.
	 */
	@Path("/{id}/gene")
	@GET
	public Response getGenes(
		@PathParam("id") final int genomeId
	) {
		try {
			final TestGenome test = genomeController.getTest(genomeId);
				
			if (test == null) {
				return createBadRequestResponse("No genome found.");
			} else {
				final String login = test.getUser().getLogin();
				
				return ServiceUtils.doIfPrivileged(
					this.security,
					login,
					() -> Response.ok(
						new GeneNames(test.getGenes())
					).build(),
					e -> {
						LOG.error("Error retrieving genes", e);
						
						return createInternalServerErrorResponse(
							"Error retrieving genes: " + e.getMessage()
						);
					},
					() -> {
						LOG.warn(String.format(
							"Illegal access request of user %s to genome %d",
							login, genomeId
						));
						
						return createBadRequestResponse("No genome found.");
					}
				);
			}
		} catch (Exception e) {
			LOG.error("Error retrieving genes", e);
			
			return createInternalServerErrorResponse(
				"Error retrieving genes: " + e.getMessage()
			);
		}
	}
	
	/**
	 * Returns the name and sequence of a gene.
	 * 
	 * @param genomeId Id of the test genome to which the gene belongs.
	 * @param geneId Name of the gene.
	 * @return The name and sequence of the gene with the {@code geneId} name
	 * that belongs to the genome identified by {@code genomeId} with a 200 OK
	 * HTTP status if the request was successful, an error message with a 400
	 * Bad Request HTTP status if the user does not have a genome with the
	 * provided id or if the genome does not have a gene with the provided
	 * name, or an error message with a 500 Internal Server Error HTTP status
	 * if an error occurs.
	 */
	@Path("/{genomeId}/gene/{geneName}")
	@GET
	public Response getGene(
		@PathParam("genomeId") int genomeId,
		@PathParam("geneName") String geneName
	) {
		try {
			final TestGenome test = genomeController.getTest(genomeId);
			
			if (test == null) {
				return createBadRequestResponse("No gene found.");
			} else {
				final String login = test.getUser().getLogin();
				
				return doIfPrivileged(
					this.security,
					login,
					() -> {
						final TestGene gene = test.getGenes().stream()
							.filter(g -> g.getName().equals(geneName))
						.findFirst()
						.orElseThrow(
							() -> new IllegalArgumentException(
								"No gene found with name " + geneName)
						);
						
						return Response.ok(
							new Gene(
								gene.getName(),
								genomeController.getTestGeneSequence(gene)
							)
						).build();
					},
					e -> {
						LOG.error("Error retrieving gene " + geneName, e);
						
						return createInternalServerErrorResponse(
							"Error retrieving gene " + e.getMessage()
						);
					},
					() -> {
						LOG.warn(String.format(
							"Illegal access request of user %s to gene %s of genome %d",
							login, geneName, genomeId
						));
						
						return createBadRequestResponse("No gene found.");
					}
				);
			}
		} catch (Exception e) {
			LOG.error("Error retrieving gene " + geneName, e);
			
			return createInternalServerErrorResponse(
				"Error retrieving gene " + e.getMessage()
			);
		}
	}
}
