/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.util;

import java.util.Objects;

/**
 * Wrapper runtime exception useful for methods used with the stream API that
 * want to throw an exception but they are not allowed. The non-runtime 
 * exception can be wrapped with this exception, catch outside the stream
 * invocations and unwrapped to be treated as a regular exception.
 * 
 * @author Miguel Reboiro-Jato
 *
 */
public class WrapperRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public static <T> T wrap(ThrowingSupplier<T> supplier)
	throws WrapperRuntimeException {
		try {
			return supplier.get();
		} catch (Throwable e) {
			throw new WrapperRuntimeException(e);
		}
	}
	
	public static WrapperRuntimeException wrap(Throwable wrapped) {
		return new WrapperRuntimeException(wrapped);
	}
	
	/**
	 * Constructs a new instance of <code>WrapperRuntimeException</code> that
	 * wraps another exception.
	 * 
	 * @param wrapped the wrapped exception. Can't be <code>null</code>.
	 */
	public WrapperRuntimeException(Throwable wrapped) {
		super(Objects.requireNonNull(wrapped));
	}
	
	/**
	 * Returns the wrapped exception casted to the type provided as input 
	 * argument.
	 * 
	 * @param causeType the type to cast the wrapped exception to.
	 * @param <T> the return type of this method. Wrapped exception will be 
	 * casted to this type.
	 * @return the wrapped exception casted to the input type.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Throwable> T getWrappedAs(Class<T> causeType) {
		return (T) super.getCause();
	}
	
	/**
	 * Rethrows the wrapped exception if its class is the same as the class
	 * passed as argument.
	 * 
	 * @param exceptionType the type of exception checked.
	 * @param <T> the expected type of the wrapped exception.
	 * @throws T if the wrapped exception is an instance of 
	 * {@code exceptionType}.
	 * @return this instance to allow fluid-style method invocation.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Throwable> WrapperRuntimeException rethrowIfIs(Class<T> exceptionType) 
	throws T {
		if (exceptionType.isInstance(getCause()))
			throw (T) this.getCause();
		else
			return this;
	}
}
