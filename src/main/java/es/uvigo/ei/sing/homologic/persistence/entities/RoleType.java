/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.persistence.entities;

import java.util.stream.Stream;

public enum RoleType {
	ADMIN("hl-admin"), USER("hl-user");
	
	private final String id;
	
	private RoleType(String id) {
		this.id = id;
	}
	
	public String id() {
		return this.id;
	}
	
	public static RoleType forId(String id) {
		return Stream.of(values())
			.filter(role -> role.id().equals(id))
			.findFirst()
		.orElseThrow(() -> new IllegalArgumentException("Unknown id: " + id));
	}
}
