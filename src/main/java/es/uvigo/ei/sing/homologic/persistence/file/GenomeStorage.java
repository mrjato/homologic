/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.persistence.file;

import static es.uvigo.ei.sing.homologic.util.ExceptionUtils.errorLogAndThrow;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import es.uvigo.ei.sing.homologic.util.Configuration;
import es.uvigo.ei.sing.homologic.util.IOUtils;
import es.uvigo.ei.sing.homologic.util.TypedThrowingFunction;

@Singleton
@Resource
@Component
public class GenomeStorage {
	private final static Logger LOG = LoggerFactory.getLogger(GenomeStorage.class);
	
	@Inject
	private Configuration configuration;

	public String storeTest(String login, InputStream is)
	throws StorageException {
		return store(uuid -> getTestPath(login, uuid), is);
	}

	public String storeReference(InputStream is)
	throws StorageException {
		return store(this::getReferencePath, is);
	}
	
	public File getTest(String login, String uuid)
	throws StorageException {
		return get(getTestPath(login, uuid));
	}
	
	public File getReference(String uuid)
	throws StorageException {
		return get(getReferencePath(uuid));
	}
	
	public boolean deleteTest(String login, String uuid)
	throws StorageException {
		return delete(getTest(login, uuid));
	}
	
	public boolean deleteReference(String uuid)
	throws StorageException {
		return delete(this.getReference(uuid));
	}
	
	public String getGeneSequence(String login, String uuid, long start, long length)
	throws StorageException {
		try (final RandomAccessFile raf = new RandomAccessFile(getTest(login, uuid), "r")) {
			//TODO: Add support for big files.
			raf.skipBytes((int) start);
			final byte[] buffer = new byte[(int) length];
			raf.readFully(buffer);
			
			return new String(buffer);
		} catch (IOException ioe) {
			throw logAndCreateException(
				"Error retrieving gene sequence", ioe);
		}
	}

	private Path getTestPath(String login, String uuid)
	throws StorageException {
		final Path path = configuration.getTestGenomeDirectoryPath()
			.resolve(login);
		
		try {
			Files.createDirectories(path);
		} catch (IOException e) {
			throw logAndCreateException(
				"Error retrieving test file: " + path, e);
		}
		
		return path.resolve(uuid);
	}

	private Path getReferencePath(String uuid)
	throws StorageException {
		final Path path = configuration.getReferenceGenomeDirectoryPath();
		
		try {
			Files.createDirectories(path);
		} catch (IOException e) {
			throw logAndCreateException(
				"Error retrieving file file: " + path, e);
		}
		
		return path.resolve(uuid);
	}

	private String store(
		TypedThrowingFunction<String, Path, StorageException> uuidToPath,
		InputStream is
	) throws StorageException {
		final String uuid = UUID.randomUUID().toString();
		
		try {
			final Path path = uuidToPath.apply(uuid);
		
			try (FileOutputStream fos = new FileOutputStream(path.toFile())) {
				IOUtils.copy(is, fos);
			}
		} catch (IOException ioe) {
			throw logAndCreateException(
				"Unable to store file with uuid: " + uuid, ioe);
		}
		
		return uuid;
	}

	private File get(Path path) throws StorageException {
		if (Files.isReadable(path)) {
			return path.toFile();
		} else {
			throw logAndCreateException("File not found: " + path.toString());
		}
	}

	private boolean delete(File file) {
		return file.delete();
	}
	
	private StorageException logAndCreateException(String message) {
		return errorLogAndThrow(
			LOG, message, 
			StorageException::new
		);
	}
	
	private StorageException logAndCreateException(String message, Exception e) {
		return errorLogAndThrow(
			LOG, message, e,
			StorageException::new
		);
	}
}
