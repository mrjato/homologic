/**
 *  HomoLogic
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.persistence.entities;

import static es.uvigo.ei.sing.homologic.util.Checks.requireNonNegative;
import static es.uvigo.ei.sing.homologic.util.Checks.requirePositive;
import static es.uvigo.ei.sing.homologic.util.Checks.requireStringSize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class TestGene {
	@Id
	@GeneratedValue
	protected Integer id;
	
	@Column(length = 1024, nullable = false)
	@NotNull(message = "Name can not be null")
	@Size(min = 1, max = 1024, message = "Name can not be empty or have more than 1024 characters")
	private String name;

	@Column(nullable = false)
	@NotNull(message = "Start can not be null")
	@Min(value = 0, message = "Start must be greater than or equals to 0")
	private long start;

	@Column(nullable = false)
	@NotNull(message = "Length can not be null")
	@Min(value = 1, message = "Length must be greater than 0")
	private int length;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private TestGenome genome;
	
	TestGene() {
	}
	
	public TestGene(String name, long start, int length, TestGenome genome) {
		this.setName(name);
		this.setStart(start);
		this.setLength(length);
		this.setGenome(genome);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = requireStringSize(name, 1, 1024,
			"Name can not be empty or have more than 1024 characters");
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = requireNonNegative(start, "Start must be greater than 0");
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = requirePositive(length, "Length must be greater than 0");
	}

	public TestGenome getGenome() {
		return this.genome;
	}

	public void setGenome(TestGenome genome) {
		if (this.genome != null) {
			this.genome._removeGene(this);
		}

		this.genome = genome;

		if (this.genome != null) {
			this.genome._addGene(this);
		}
	}
}
