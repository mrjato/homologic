/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.services.entities;

import static es.uvigo.ei.sing.homologic.util.Checks.requireStringSize;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "user-login", namespace = "http://sing.ei.uvigo.es/homologic")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserLogin {
	@NotNull(message = "Login can not be null")
	@Size(min = 1, max = 50, message = "Login must have between 1 and 50 characters")
	@XmlValue
	private String login;

	UserLogin() {}
	
	public UserLogin(String login) {
		this.setLogin(login);
	}

	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = requireStringSize(login, 1, 50, 
			"Login must have between 1 and 50 characters");
	}
}
