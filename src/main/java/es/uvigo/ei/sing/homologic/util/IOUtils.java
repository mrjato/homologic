/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class IOUtils {
	private IOUtils() {
	}
	
	public static void copy(InputStream is, OutputStream os)
	throws IOException {
		final byte[] buffer = new byte[4096];
		
		int length;
		while ((length = is.read(buffer)) != -1) {
			os.write(buffer, 0, length);
		}
	}
	
	public static void copyAndClose(InputStream is, OutputStream os)
	throws IOException {
		try (InputStream is2 = is; OutputStream os2 = os) {
			copy(is, os);
		}
	}
}
