/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.services.entities;

import static es.uvigo.ei.sing.homologic.util.Checks.EMAIL_PATTERN;
import static es.uvigo.ei.sing.homologic.util.Checks.MD5_PATTERN;
import static es.uvigo.ei.sing.homologic.util.Checks.requireEmail;
import static es.uvigo.ei.sing.homologic.util.Checks.requireMD5;
import static es.uvigo.ei.sing.homologic.util.Checks.requireStringSize;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import es.uvigo.ei.sing.homologic.persistence.entities.RoleType;
import es.uvigo.ei.sing.homologic.persistence.entities.TestGenome;
import es.uvigo.ei.sing.homologic.persistence.entities.User;

@XmlRootElement(name = "user-metadata", namespace = "http://sing.ei.uvigo.es/homologic")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserMetadata {
	@NotNull(message = "Login can not be null")
	@Size(min = 1, max = 50, message = "Login must have between 1 and 50 characters")
	@XmlElement(required = true, nillable = false)
	private String login;
	
	@NotNull
	@Pattern(regexp = EMAIL_PATTERN, message = "Invalid email address")
	@Size(min = 5, max = 100, message = "Invalid email address")
	@XmlElement(required = true, nillable = false)
	private String email;

	@NotNull(message = "Password can not be null")
	@Size(min = 32, max = 32, message = "Password must be MD5 digested")
	@Pattern(regexp = MD5_PATTERN, message = "Password must be MD5 digested")
	@XmlElement(required = true, nillable = false)
	private String password;
	
	@NotNull(message = "Role id can not be null")
	@Enumerated(EnumType.STRING)
	@XmlElement(required = true, nillable = false)
	private RoleType role;
	
	@NotNull
    @XmlElementWrapper(name = "genomes")
	@XmlElement(name = "uuid", nillable = false)
	private Set<String> uuids;
	
	UserMetadata() {
		this.uuids = new HashSet<>();
	}
	
	public UserMetadata(User user, String role) {
		this(
			user.getLogin(),
			user.getEmail(),
			user.getPassword(),
			role,
			user.getGenomes().stream()
				.map(TestGenome::getUuid)
			.collect(Collectors.toSet())
		);
	}
	
	public UserMetadata(
		String login, String email, String password, String role, Set<String> uuids
	) {
		this.login = login;
		this.email = email;
		this.password = password;
		this.uuids = new HashSet<>(uuids);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = requireStringSize(login, 1, 50, "Login must have between 1 and 50 characters");
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = requireEmail(email, "Invalid email address");
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = requireMD5(password, "Invalid password");
	}

	public Set<String> getUuids() {
		return Collections.unmodifiableSet(uuids);
	}
	
	public boolean addUuid(String uuid) {
		return this.uuids.add(uuid);
	}

	public boolean removeUuid(String uuid) {
		return this.uuids.remove(uuid);
	}
	
	public boolean hasUuid(String uuid) {
		return this.uuids.contains(uuid);
	}
}
