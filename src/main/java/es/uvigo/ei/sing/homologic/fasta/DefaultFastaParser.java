/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.fasta;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class DefaultFastaParser implements FastaParser {
	protected static enum State { START, FIRST_FRAGMENT, NAME_OR_FRAGMENT };
	
	protected final List<FastaParserListener> listeners;
	
	public DefaultFastaParser() {
		this.listeners = new CopyOnWriteArrayList<>();
	}
	
	@Override
	public void parse(File file) throws FastaParseException, IOException {
		try (final IndexerReader br = new DefaultIndexedReader(new FileReader(file))) {
			String line;
			
			State state = State.START;
			
			long currentEnd = 0l;
			
			notifyParseStart(file);
			while ((line = br.readLine()) != null) {
				line = line.trim();
				
				if (line.isEmpty()) {
					throwInvalidFormatException(file, br.getLineNumber(), "empty line");
				} else {
					switch (state) {
						case START:
							if (line.startsWith(">")) {
								notifySequenceStart(file, br.getLineStart());
								notifySequenceNameRead(file, line, br.getLineStart());
								state = State.FIRST_FRAGMENT;
							} else {
								throwInvalidFormatException(file, br.getLineNumber(), "missing sequence name");
							}
							break;
						case FIRST_FRAGMENT:
							if (line.startsWith(">")) {
								throwInvalidFormatException(file, br.getLineNumber(), "missing sequence");							
							} else {
								currentEnd = br.getLineEnd();
								notifySequenceFragmentRead(file, line, br.getLineStart());
								state = State.NAME_OR_FRAGMENT;
							}
							break;
						case NAME_OR_FRAGMENT:
							if (line.startsWith(">")) {
								notifySequenceEnd(file, currentEnd);
								notifySequenceStart(file, br.getLineStart());
								notifySequenceNameRead(file, line, br.getLineStart());
								state = State.FIRST_FRAGMENT;		
							} else {
								currentEnd = br.getLineEnd();
								notifySequenceFragmentRead(file, line, br.getLineStart());
							}
					}
				}
			}
			switch(state) {
			case START:
				throw new IOException(String.format("File '%s' is empty.", file.getAbsolutePath()));
			case FIRST_FRAGMENT:
				throwInvalidFormatException(file, br.getLineNumber(), "last sequence is incomplete");				
			case NAME_OR_FRAGMENT:
				notifySequenceEnd(file, currentEnd);
			}
			
			notifyParseEnd(file);
		}
	}
	
	protected void throwInvalidFormatException(File file, long lineCount, String formatError) 
	throws FastaParseException {
		throw new FastaParseException(
			String.format("File '%s' has an invalid fasta format. Line %d is invalid: %s.", 
				file.getAbsolutePath(), lineCount, formatError
			)
		);
	}
	
	protected void notifySequenceNameRead(File file, String sequenceName, long start) 
	throws FastaParseException {
		for (FastaParserListener listener : this.listeners) {
			listener.sequenceNameRead(file, sequenceName, start);
		}
	}
	
	protected void notifySequenceFragmentRead(File file, String sequence, long start) 
	throws FastaParseException {
		for (FastaParserListener listener : this.listeners) {
			listener.sequenceFragmentRead(file, sequence, start);
		}
	}
	
	protected void notifyParseStart(File file) 
	throws FastaParseException {
		for (FastaParserListener listener : this.listeners) {
			listener.parseStart(file);
		}
	}
	
	protected void notifyParseEnd(File file) 
	throws FastaParseException {
		for (FastaParserListener listener : this.listeners) {
			listener.parseEnd(file);
		}
	}
	
	protected void notifySequenceStart(File file, long start) 
	throws FastaParseException {
		for (FastaParserListener listener : this.listeners) {
			listener.sequenceStart(file, start);
		}
	}
	
	protected void notifySequenceEnd(File file, long end) 
	throws FastaParseException {
		for (FastaParserListener listener : this.listeners) {
			listener.sequenceEnd(file, end);
		}
	}

	@Override
	public void addParseListener(FastaParserListener listener) {
		this.listeners.add(listener);
	}

	@Override
	public void removeParseListener(FastaParserListener listener) {
		this.listeners.remove(listener);
	}

	@Override
	public void containsParseListener(FastaParserListener listener) {
		this.listeners.contains(listener);
	}
}
