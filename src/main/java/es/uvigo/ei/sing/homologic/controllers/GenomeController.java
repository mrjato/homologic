/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.controllers;

import static java.util.Objects.requireNonNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import es.uvigo.ei.sing.homologic.fasta.DefaultFastaParser;
import es.uvigo.ei.sing.homologic.fasta.FastaParseException;
import es.uvigo.ei.sing.homologic.fasta.FastaParserListener;
import es.uvigo.ei.sing.homologic.persistence.daos.ReferenceGenomeDAO;
import es.uvigo.ei.sing.homologic.persistence.daos.TestGenomeDAO;
import es.uvigo.ei.sing.homologic.persistence.daos.UserDAO;
import es.uvigo.ei.sing.homologic.persistence.entities.ReferenceGenome;
import es.uvigo.ei.sing.homologic.persistence.entities.TestGene;
import es.uvigo.ei.sing.homologic.persistence.entities.TestGenome;
import es.uvigo.ei.sing.homologic.persistence.entities.User;
import es.uvigo.ei.sing.homologic.persistence.file.GenomeStorage;
import es.uvigo.ei.sing.homologic.persistence.file.StorageException;

@Controller
@Transactional
@Lazy
public class GenomeController {
	@Inject
	private GenomeStorage genomeStorage;
	
	@Inject
	private UserDAO userDAO;
	
	@Inject
	private TestGenomeDAO testGenomeDAO;
	
	@Inject
	private ReferenceGenomeDAO referenceGenomeDAO;
	
	public String storeTest(String login, InputStream is)
	throws StorageException {
		return this.genomeStorage.storeTest(login, is);
	}
	
	public TestGenome createTest(String login, String genomeName, String uuid)
	throws FastaParseException, StorageException, IOException {
		final File genomeFile = this.genomeStorage.getTest(login, uuid);
		final List<TestGene> listGenomeGenes = listGenomeGenes(genomeFile);
		
		final User user = userDAO.get(login);
		return this.testGenomeDAO.persist(
			new TestGenome(genomeName, uuid, user, listGenomeGenes)
		);
	}

	public TestGenome getTest(int genomeId) {
		return this.testGenomeDAO.get(genomeId);
	}

	public String getTestGeneSequence(TestGene gene)
	throws IOException {
		return this.genomeStorage.getGeneSequence(
			gene.getGenome().getUser().getLogin(),
			gene.getGenome().getUuid(),
			gene.getStart(),
			gene.getLength()
		);
	}
	
	public void deleteTest(String login, int genomeId)
	throws StorageException, IOException {
		final TestGenome genome = requireNonNull(
			testGenomeDAO.get(genomeId),
			"No genome found with id: " + genomeId
		);
		
		final String ownerLogin = genome.getUser().getLogin();
		
		if (ownerLogin.equals(login)) {
			genome.setUser(null);
			testGenomeDAO.remove(genome);
			
			if (!genomeStorage.deleteTest(ownerLogin, genome.getUuid())) {
				throw new IOException("Error deleting genome");
			}
		} else {
			throw new IllegalArgumentException(
				"No genome found with id: " + genomeId);
		}
	}
	
	public List<TestGenome> listTestFor(String userLogin) {
		return this.testGenomeDAO.list(userDAO.get(userLogin));
	}
	
	public String storeReference(InputStream is) throws IOException {
		return this.genomeStorage.storeReference(is);
	}
	
	public ReferenceGenome createReference(String genomeName, String uuid)
	throws FastaParseException, IOException, StorageException {
		final File genomeFile = this.genomeStorage.getReference(uuid);
		
		final int numGenes = countGenomeGenes(genomeFile);
		
		return this.referenceGenomeDAO.persist(
			new ReferenceGenome(genomeName, uuid, numGenes)
		);
	}

	public ReferenceGenome getReference(int genomeId) {
		return this.referenceGenomeDAO.get(genomeId);
	}
	
	public void deleteReference(int genomeId) throws IOException {
		final ReferenceGenome genome = requireNonNull(
			referenceGenomeDAO.get(genomeId),
			"No genome found with id: " + genomeId
		);
		
		referenceGenomeDAO.remove(genome);
			
		if (!genomeStorage.deleteReference(genome.getUuid())) {
			throw new IOException("Error deleting genome");
		}
	}
	
	public List<ReferenceGenome> listReference() {
		return this.referenceGenomeDAO.list();
	}

	private static int countGenomeGenes(File genome)
	throws FastaParseException, IOException {
		final DefaultFastaParser fastaParser = new DefaultFastaParser();
		
		final AtomicInteger numGenes = new AtomicInteger(0);
		fastaParser.addParseListener(new FastaParserListener() {
			@Override
			public void sequenceStart(File file, long start) throws FastaParseException {
				numGenes.incrementAndGet();
			}
		});
		fastaParser.parse(genome);
		
		return numGenes.get();
	}

	private static List<TestGene> listGenomeGenes(File genome)
	throws FastaParseException, IOException {
		final DefaultFastaParser fastaParser = new DefaultFastaParser();
		
		final List<TestGene> testGenes = new LinkedList<TestGene>();
		fastaParser.addParseListener(new FastaParserListener() {
			private String name;
			private long start;
			
			@Override
			public void sequenceNameRead(
				File file, String sequenceName, long start
			) {
				this.name = sequenceName.substring(1);
				this.start = Long.MIN_VALUE;
			}
			
			@Override
			public void sequenceFragmentRead(
				File file, String sequence, long start
			) {
				if (this.start == Long.MIN_VALUE) {
					this.start = start;
				}
			}
			
			@Override
			public void sequenceEnd(File file, long end)
			throws FastaParseException {
				testGenes.add(new TestGene(name, start, (int) (end - start), null));
			};
		});
		fastaParser.parse(genome);
		
		return testGenes;
	}
}
