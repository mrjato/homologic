/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.services;

import static es.uvigo.ei.sing.homologic.services.ServiceUtils.createInternalServerErrorResponse;
import static es.uvigo.ei.sing.homologic.services.ServiceUtils.createNotFoundResponse;
import static es.uvigo.ei.sing.homologic.services.ServiceUtils.createUnauthorizedResponse;
import static es.uvigo.ei.sing.homologic.services.ServiceUtils.doIfPrivileged;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import es.uvigo.ei.sing.homologic.controllers.UserController;
import es.uvigo.ei.sing.homologic.persistence.entities.User;
import es.uvigo.ei.sing.homologic.services.entities.Message;
import es.uvigo.ei.sing.homologic.services.entities.UserLogin;
import es.uvigo.ei.sing.homologic.services.entities.UserMetadata;

@Path("users")
@Service
@RolesAllowed("hl-admin")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class UserService {
	private final static Logger LOG = LoggerFactory.getLogger(UserService.class);
	
	@Inject
	private UserController controller;
	
	@Context
	private SecurityContext security;
	
	/**
	 * <p>
	 * Provides information about an user.
	 * </p>
	 * <p>
	 * Administrator users can access the information of any user. Normal users
	 * can only access to their own information.
	 * </p>
	 * 
	 * @param login Login of the user.
	 * @return The information of the user with a 200 OK HTTP status if the 
	 * client has permission to access that user information. A 401 
	 * Unauthorized HTTP  status if the client doesn't have permission to 
	 * access the user information (even if the requested user doesn't exists).
	 * A 404 Not Found HTTP status if the requested user doesn't exists (this 
	 * will only happen for administrator users). A 500 Internal Server Error
	 * HTTP status if an error occurs.
	 */
	@GET
	@Path("/{login}")
	@RolesAllowed({ "hl-admin", "hl-user" })
	public Response get(
		@PathParam("login") UserLogin login
	) {
		final String userLogin = login.getLogin();
		
		return doIfPrivileged(
			security,
			userLogin,
			() -> {
				final User user = controller.get(userLogin);
				
				if (user == null) {
					return createNotFoundResponse(
						"No user found with login: " + userLogin
					);
				} else {
					return Response.ok(
						createUserMetadata(user)
					).build();
				}
			},
			e -> {
				LOG.error("Error while retrieving user information: " + userLogin, e);
				
				return createInternalServerErrorResponse(
					"Error while retrieving user information: " + userLogin
				);
			},
			() -> {
				LOG.error(String.format(
					"Illegal access attempt of user %s to data of user %s",
					ServiceUtils.getUserName(security), userLogin
				));
				
				return createUnauthorizedResponse("Unauthorized access.");
			}
		);
	}
	
	/**
	 * <p>
	 * Updates the user profile data. Only the email and password of the users
	 * can be updated.
	 * </p>
	 * <p>
	 * Administrator users can access the information of any user. Normal users
	 * can only access to their own information.
	 * </p>
	 * 
	 * @param userMetadata New profile data of the user.
	 * @return The information of the user updated with a 200 OK HTTP status if
	 * the client has permission to access that user information. A 401 
	 * Unauthorized HTTP  status if the client doesn't have permission to 
	 * access the user information (even if the requested user doesn't exists).
	 * A 404 Not Found HTTP status if the requested user doesn't exists (this 
	 * will only happen for administrator users). A 500 Internal Server Error
	 * HTTP status if an error occurs.
	 */
	@PUT
	@RolesAllowed({ "hl-admin", "hl-user" })
	public Response update(
		UserMetadata userMetadata
	) {
		final String userLogin = userMetadata.getLogin();
		
		return doIfPrivileged(
			security, 
			userLogin, 
			() -> {
				final User user = controller.get(userLogin);
				
				if (user == null) {
					return createNotFoundResponse(
						"No user found with login: " + userLogin
					);
				} else {
					user.setEmail(userMetadata.getEmail());
					user.setPassword(userMetadata.getPassword());

					return Response.ok(
						createUserMetadata(controller.update(user))
					).build();
				}
			},
			e -> {
				LOG.error("Error while updating user information: " + userLogin, e);
				
				return createInternalServerErrorResponse(
					"Error while updating user information: " + userLogin
				);
			},
			() -> {
				LOG.error(String.format(
					"Illegal update attempt of user %s to data of user %s",
					ServiceUtils.getUserName(security), userLogin
				));
				
				return createUnauthorizedResponse("Unauthorized access.");
			}
		);
	}
	
	/**
	 * Deletes an user from the system.
	 * 
	 * <p>This method can be invoked only by admin users.</p>
	 * 
	 * @param login Login of the user to be deleted.
	 * @return A confirmation message with a 200 OK HTTP status if deletion was
	 * successful or an error message with a 500 Internal Server Error HTTP
	 * status if an error occurs.
	 */
	@DELETE
	public Response delete(
		@PathParam("login") UserLogin login
	) {
		try {
			this.controller.delete(login.getLogin());
	
			return Response.ok(
				new Message("User " + login + " deleted")
			).build();
		} catch (Exception e) {
			LOG.error("Error deleting user", e);
			
			return ServiceUtils.createErrorResponse(
				INTERNAL_SERVER_ERROR, "Error deleting user"
			);
		}
	}

	/**
	 * Returns the list of users registered in the system.
	 * 
	 * <p>This method can be invoked only by admin users.</p>
	 * 
	 * @return The list of users in the system with a 200 OK HTTP status if
	 * the client has permission to access that user information. A 401 
	 * Unauthorized HTTP status if the client doesn't have permission to 
	 * access the list of users.
	 */
	@GET
	public Response list() {
		return Response.ok(
			controller.list().stream()
				.map(this::createUserMetadata)
			.collect(toList())
		).build();
	}
	
	private UserMetadata createUserMetadata(User user) {
		return new UserMetadata(user, controller.getUserRole(user).id());
	}
}
