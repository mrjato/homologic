/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.persistence.entities;

import static es.uvigo.ei.sing.homologic.util.Checks.UUID_PATTERN;
import static es.uvigo.ei.sing.homologic.util.Checks.requirePositive;
import static es.uvigo.ei.sing.homologic.util.Checks.requireStringSize;
import static es.uvigo.ei.sing.homologic.util.Checks.requireUUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Genome {
	@Id
	@GeneratedValue
	protected Integer id;

	@Column(length = 1024)
	@NotNull(message = "Name can not be null")
	@Size(min = 1, max = 1024, message = "Name can not be empty or have more than 1024 characters")
	protected String name;
	
	@Column(length = 36, unique = true)
	@NotNull(message = "UUID can not be null")
	@Pattern(regexp = UUID_PATTERN, message = "Invalid UUID")
	protected String uuid;
	
	@Column(nullable = false)
	@NotNull(message = "Number of genes can not be null")
	@Min(value = 1, message = "Number of genes must be greater than 0")
	protected int numGenes;
	
	Genome() {
	}

	public Genome(String name, String uuid, int numGenes) {
		this.setName(name);
		this.setUuid(uuid);
		this.setNumGenes(numGenes);
	}
	
	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = requireStringSize(name, 1, 1024,
			"Name can not be empty or have more than 1024 characters");
	}
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = requireUUID(uuid, "Invalid UUID");
	}

	public int getNumGenes() {
		return numGenes;
	}

	public void setNumGenes(int numGenes) {
		this.numGenes = requirePositive(numGenes, "Number of genes must be greater than 0");
	}
}
