/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.persistence.entities;

import static es.uvigo.ei.sing.homologic.util.Checks.EMAIL_PATTERN;
import static es.uvigo.ei.sing.homologic.util.Checks.MD5_PATTERN;
import static es.uvigo.ei.sing.homologic.util.Checks.UUID_PATTERN;
import static es.uvigo.ei.sing.homologic.util.Checks.requireEmail;
import static es.uvigo.ei.sing.homologic.util.Checks.requireMD5;
import static es.uvigo.ei.sing.homologic.util.Checks.requireStringSize;
import static es.uvigo.ei.sing.homologic.util.Checks.requireUUID;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class Registration {
	@Id
	@Column(length = 36)
	@NotNull
	@Size(min = 36, max = 36, message = "Invalid registration UUID")
	@Pattern(regexp = UUID_PATTERN)
	private String uuid;
	
	@Column(unique = true, updatable = false, length = 50)
	@NotNull
	@Size(min = 1, max = 50, message = "Login must have between 1 and 50 characters")
	private String login;
	
	@Column(unique = true, length = 100)
	@NotNull
	@Pattern(regexp = EMAIL_PATTERN, message = "Invalid email address")
	@Size(min = 5, max = 100, message = "Invalid email address")
	private String email;
	
	@Column(length = 32)
	@NotNull
	@Size(min = 32, max = 32, message = "Password must be MD5 digested")
	@Pattern(regexp = MD5_PATTERN, message = "Password must be MD5 digested")
	private String password;
	
	Registration() {
	}
	
	public Registration(String login, String email, String password) {
		this.setLogin(login);
		this.setEmail(email);
		this.setPassword(password);
		this.uuid = UUID.randomUUID().toString();
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = requireStringSize(login, 1, 50, "Login must have between 1 and 50 characters");
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = requireEmail(email, "Invalid email address");
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = requireMD5(password, "Password must be MD5 digested");
	}
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = requireUUID(uuid, "Invalid registration UUID");
	}
	
	public User toUser() {
		return new User(this.getLogin(), this.getEmail(), this.getPassword());
	}
}
