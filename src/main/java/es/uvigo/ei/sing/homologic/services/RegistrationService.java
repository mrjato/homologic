/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.services;

import static es.uvigo.ei.sing.homologic.services.ServiceUtils.createBadRequestResponse;
import static es.uvigo.ei.sing.homologic.services.ServiceUtils.createNotFoundResponse;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Service;

import es.uvigo.ei.sing.homologic.controllers.RegistrationController;
import es.uvigo.ei.sing.homologic.persistence.entities.Registration;
import es.uvigo.ei.sing.homologic.services.entities.UUID;

@Path("registration")
@Service
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class RegistrationService {
	@Inject
	private RegistrationController controller;
	
	/**
	 * Creates a new normal user registration request. An UUID will be 
	 * generated that user must use to access the application.
	 * 
	 * The confirmation path is:
	 * {@code http://&lt;server&gt;/public/registration/confirm/&lt;uuid&gt;}
	 * 
	 * @param registration Data for user registration.
	 * @return The generated UUID with a 200 OK HTTP status if registration was
	 * successful or an error message with a 400 Bad Request HTTP status if an 
	 * error occurs. 
	 */
	@POST
	public Response register(
		Registration registration
	) {
		try {
			return Response.ok(new UUID(controller.register(
				registration.getLogin(),
				registration.getEmail(),
				registration.getPassword()
			).getUuid())).build();
		} catch (IllegalArgumentException iae) {
			return createBadRequestResponse(iae);
		}
	}
	
	/**
	 * Confirms an user registration. If the UUID exists, the associated user 
	 * will be effectively created as a regular user.
	 * 
	 * @param uuid UUID generated during registration.
	 * @return The new user created after confirmation with a 200 OK HTTP 
	 * status. If the uuid is not valid, then an "Invalid UUID" message will be
	 * returned with a 404 Not Found HTTP status. 
	 */
	@GET
	@Path("/{uuid}")
	@Consumes(MediaType.WILDCARD)
	public Response confirm(
		@PathParam("uuid") UUID uuid
	) {
		try {
			return Response.ok(controller.confirm(uuid.getUuid())).build();
		} catch (IllegalArgumentException iae) {
			return createNotFoundResponse(iae);
		}
	}
}
