/**
 *  HomoLogic
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.services.entities;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import es.uvigo.ei.sing.homologic.persistence.entities.TestGene;

@XmlRootElement(name = "gene-names", namespace = "http://sing.ei.uvigo.es/homologic")
@XmlAccessorType(XmlAccessType.FIELD)
public class GeneNames {
	@NotNull
	@XmlElement(name = "gene", nillable = false)
	private List<String> names;
	
	GeneNames() {
	}
	
	public GeneNames(Collection<TestGene> genes) {
		this(genes.stream()
			.map(TestGene::getName)
		.collect(Collectors.toList()));
	}

	public GeneNames(List<String> names) {
		this.names = names;
	}
	
	public List<String> getNames() {
		return names;
	}
	
	public boolean addNames(String name) {
		return this.names.add(name);
	}

	public boolean removeNames(String name) {
		return this.names.remove(name);
	}
	
	public boolean hasNames(String name) {
		return this.names.contains(name);
	}
}
