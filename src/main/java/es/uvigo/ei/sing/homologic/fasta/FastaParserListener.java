/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.fasta;

import java.io.File;

public interface FastaParserListener {
	public default void parseStart(File file) throws FastaParseException {
	}

	public default void sequenceStart(File file, long start) throws FastaParseException {
	}

	public default void sequenceNameRead(File file, String sequenceName, long start)
	throws FastaParseException {
	}

	public default void sequenceFragmentRead(File file, String sequence, long start)
	throws FastaParseException {
	}

	public default void sequenceEnd(File file, long end) throws FastaParseException {
	}

	public default void parseEnd(File file) throws FastaParseException {
	}
}
