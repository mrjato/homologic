/**
 *  HomoLogic
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.fasta;

import static es.uvigo.ei.sing.homologic.util.Checks.requirePositive;

import java.io.IOException;
import java.io.Reader;

public class DefaultIndexedReader implements IndexerReader {
	private final Reader reader;
	private final char[] buffer;
	
	private long lineNumber;
	private long lineStart;
	private long lineEnd;
	private long count;
	
	private int head;
	private int length;
	
	public DefaultIndexedReader(Reader reader) {
		this(reader, 8192);
	}
	
	public DefaultIndexedReader(Reader reader, int bufferSize) {
		this.reader = reader;
		this.buffer = new char[requirePositive(bufferSize, "bufferSize must be greater than 0")];
		this.head = 0;
		this.length = 0;
		
		this.lineNumber = 0;
		this.lineStart = 0;
		this.lineEnd = 0;
		this.count = 0;
	}
	
	private int read() throws IOException {
		if (fillIfNeeded()) {
			count++;
			return buffer[head++];
		} else {
			return -1;
		}
	}
	
	private int peek() throws IOException {
		return peek(0);
	}
	
	private int peek(int n) throws IOException {
		if (n == 0) {
			return fillIfNeeded() ? buffer[head] : -1;
		} else if (fill()) {
			return n < length ? buffer[head + n] : -1;
		} else {
			return -1;
		}
	}
	
	private boolean fillIfNeeded() throws IOException {
		if (isEOF()) {
			return false;
		} else if (head == length) {
			return fill();
		} else {
			return true;
		}
	}
	
	private boolean fill() throws IOException {
		if (head < length) {
			if (head == 0) {
				return true;
			} else {
				final int unread = length - head;
				final char[] tmp = new char[unread];
				System.arraycopy(buffer, head, tmp, 0, unread);
				System.arraycopy(tmp, 0, head, 0, unread);
				
				final int readLength =
					this.reader.read(buffer, length, buffer.length - unread);
				
				length = unread;
				if (readLength >= 0) length += readLength;
				
				this.head = 0;
				return true;
			}
		} else {
			length = reader.read(buffer, 0, buffer.length);
			
			this.head = 0;
			return this.length >= 0;
		}
	}
	
	private boolean isEOF() throws IOException {
		return this.length == -1;
	}
	
	private boolean nextIsNewLine() throws IOException {
		if (isEOF()) {
			return false;
		} else {
			final char c = (char) peek();
			
			switch (c) {
			case '\n':
				read();
				
				if (peek() == '\r')
					read();
				
				return true;
			case '\r':
				if (isEOF() || peek(1) != '\n') {
					return false;
				} else {
					read(); read();
					return true;
				}
			default:
				return false;
			}
		}
	}

	@Override
	public String readLine() throws IOException {
		if (isEOF())
			return null;
		
		lineStart = count;
		final StringBuilder sb = new StringBuilder();
		do {
			sb.append((char) read());
		} while (fillIfNeeded() && !nextIsNewLine());
		
		lineEnd = count;
		lineNumber++;
		
		return sb.toString();
	}

	@Override
	public long getLineNumber() {
		return this.lineNumber;
	}

	@Override
	public long getLineStart() {
		return this.lineStart;
	}

	@Override
	public long getLineEnd() {
		return this.lineEnd;
	}

	@Override
	public void close() throws IOException {
		this.reader.close();
	}
}
