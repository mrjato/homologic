/*
 *  This file is part of HomoLogic
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.util;

import static java.util.Objects.requireNonNull;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.function.DoubleFunction;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Utility class with several methods for type checking. These methods are
 * based on the {@link Objects#requireNonNull} methods.
 * 
 * @author Miguel Reboiro-Jato
 *
 */
public final class Checks {
	public final static String EMAIL_PATTERN = 
	"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public final static String MD5_PATTERN =
	"^[a-f0-9]{32}$";
	public final static String UUID_PATTERN = 
	"^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$";

	private Checks() {}

	public static String requireEmail(String email) {
		return requireEmail(email);
	}

	public static String requireEmail(String email, String message) {
		return requirePattern(email, Checks.EMAIL_PATTERN, message);
	}

	public static String requireMD5(String md5) {
		return requireMD5(md5, null);
	}

	public static String requireMD5(String md5, String message) {
		return requirePattern(md5, MD5_PATTERN, message);
	}

	public static String requireUUID(String uuid) {
		return requireUUID(uuid, null);
	}

	public static String requireUUID(String uuid, String message) {
		return requirePattern(uuid, UUID_PATTERN, message);
	}

	public static String requirePattern(String value, String pattern, String message) {
		if (requireNonNull(value, message).matches(pattern)) {
			return value;
		} else {
			throw new IllegalArgumentException(message == null ?
				"Value did not match pattern" : message);
		}
	}
	
	public static String requireStringSize(String string, int min, int max) {
		return requireStringSize(string, min, max,
			String.format("string length must be between %d and %d", min, max)
		);
	}
	
	public static String requireStringSize(String string, int min, int max, String message) {
		return check(requireNonNull(string, message), 
			s -> s.length() >= min && s.length() <= max,
			message
		);
	}
	
	/**
	 * Checks if the provided string is not empty.
	 * 
	 * @param string the string checked.
	 * @return the provided string.
	 * @throws NullPointerException if the string is {@code null}.
	 * @throws IllegalArgumentException if the string is empty.
	 */
	public static String requireNonEmpty(String string) {
		return requireNonEmpty(string, "string can't be empty");
	}
	
	/**
	 * Checks if the provided string is not empty.
	 * 
	 * @param string the string checked.
	 * @param message the error message.
	 * @return the provided string.
	 * @throws NullPointerException if the string is {@code null}.
	 * @throws IllegalArgumentException if the string is empty.
	 */
	public static String requireNonEmpty(String string, String message) {
		return check(requireNonNull(string, message), 
			not(String::isEmpty),
			message
		);
	}
	
	/**
	 * Checks if the provided map is not empty.
	 * 
	 * @param map the map checked.
	 * @param <K> type of the map's keys.
	 * @param <V> type of the map's values.
	 * @param <M> type of the mapl
	 * @return the provided map.
	 * @throws NullPointerException if the map is {@code null}.
	 * @throws IllegalArgumentException if the map is empty.
	 */
	public static <K, V, M extends Map<K, V>> M requireNonEmpty(M map) {
		return check(requireNonNull(map), 
			not(Map::isEmpty),
			"map can't be empty"
		);
	}

	/**
	 * Checks if the provided collection is not empty.
	 * 
	 * @param collection the collection checked.
	 * @param <T> type of the collection's items.
	 * @param <C> type of the collection.
	 * @return the provided collection.
	 * @throws NullPointerException if the collection is {@code null}.
	 * @throws IllegalArgumentException if the collection is empty.
	 */
	public static <T, C extends Collection<T>> C requireNonEmpty(C collection) {
		return check(requireNonNull(collection), 
			not(Collection::isEmpty),
			"collection can't be empty"
		);
	}
	
	/**
	 * Checks if the provided array is not empty.
	 * 
	 * @param values the array checked.
	 * @param <T> type of the array items.
	 * @return the provided array.
	 * @throws NullPointerException if the array is {@code null}.
	 * @throws IllegalArgumentException if the array is empty.
	 */
	public static <T> T[] requireNonEmpty(T[] values) {
		return check(requireNonNull(values), 
			x -> x.length > 0,
			"array can't be empty"
		);
	}
	
	/**
	 * Checks if the provided array is not empty.
	 * 
	 * @param values the array checked.
	 * @return the provided array.
	 * @throws NullPointerException if the array is {@code null}.
	 * @throws IllegalArgumentException if the array is empty.
	 */
	public static int[] requireNonEmpty(int[] values) {
		return check(requireNonNull(values), 
			x -> x.length > 0,
			"array can't be empty"
		);
	}
	
	/**
	 * Checks if the provided array is neither {@code null} and not contains
	 * {@code null} values.
	 * 
	 * @param values the array to be checked.
	 * @param <T> type of the array items.
	 * @return the provided array.
	 * @throws NullPointerException if the array is {@code null}.
	 * @throws IllegalArgumentException if the array contains a {@code null}
	 * value.
	 */
	public static <T> T[] requireNonNullArray(T[] values) {
		return check(requireNonNull(values), 
			x -> Stream.of(x).allMatch(y -> y != null),
			"array can't be null and can't contain a null value"
		);
	}

	/**
	 * Checks if the provided number is not a NaN.
	 * 
	 * @param number the number to be checked.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is NaN.
	 */
	public static double requireNonNaN(double number) {
		return requireNonNaN(number, "number can't be NaN");
	}

	/**
	 * Checks if the provided number is not a NaN.
	 * 
	 * @param number the number to be checked.
	 * @param message the message of the exception thrown if the number is
	 * NaN. 
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is NaN. The message of
	 * this exception will be the provided.
	 */
	public static double requireNonNaN(double number, String message) {
		return check(number, notDouble(Double::isNaN), message);
	}

	/**
	 * Checks if the provided number is finite.
	 * 
	 * @param number the number to be checked.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is not finite.
	 */
	public static double requireFinite(double number) {
		return requireNonNegative(number, "number must be finite");
	}

	/**
	 * Checks if the provided number is finite.
	 * 
	 * @param number the number to be checked.
	 * @param message the message of the exception thrown if the number is
	 * infinite.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is infinite. The message
	 * of this exception will be the provided.
	 */
	public static double requireFinite(double number, String message) {
		return check(number, Double::isFinite, message);
	}

	/**
	 * Checks if the provided number is greater than or equals to 0.
	 * 
	 * @param number the number to be checked.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is lower than 0.
	 */
	public static double requireNonNegative(double number) {
		return requireNonNegative(number, 
			"number must be greater than or equals to 0"
		);
	}

	/**
	 * Checks if the provided number is greater than or equals to 0.
	 * 
	 * @param number the number to be checked.
	 * @param message the message of the exception thrown if the number is
	 * lower than 0.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is lower than 0. The
	 * message of this exception will be the provided.
	 */
	public static double requireNonNegative(double number, String message) {
		return check(number, x -> x >= 0, message);
	}

	/**
	 * Checks if the provided number is greater than or equals to 0.
	 * 
	 * @param number the number to be checked.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is lower than 0.
	 */
	public static int requireNonNegative(int number) {
		return requireNonNegative(number,
			"number must be greater than or equals to 0"
		);
	}

	/**
	 * Checks if the provided number is greater than or equals to 0.
	 * 
	 * @param number the number to be checked.
	 * @param message the message of the exception thrown if the number is
	 * lower than 0.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is lower than 0. The
	 * message of this exception will be the provided.
	 */
	public static int requireNonNegative(int number, String message) {
		return check(number, x -> x >= 0, message);
	}

	/**
	 * Checks if the provided number is greater than or 0.
	 * 
	 * @param number the number to be checked.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is lower than or equals
	 * to 0.
	 */
	public static long requirePositive(long number) {
		return requirePositive(number, "number must be greater than 0");
	}

	/**
	 * Checks if the provided number is greater than 0.
	 * 
	 * @param number the number to be checked.
	 * @param message the message of the exception thrown if the number is
	 * lower than or equals to 0.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is lower than 0 or equals
	 * to 0. The message of this exception will be the provided.
	 */
	public static long requirePositive(long number, String message) {
		return check(number, x -> x > 0, message);
	}

	/**
	 * Checks if the provided number is greater than or 0.
	 * 
	 * @param number the number to be checked.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is lower than or equals
	 * to 0.
	 */
	public static int requirePositive(int number) {
		return requirePositive(number, "number must be greater than 0");
	}

	/**
	 * Checks if the provided number is greater than 0.
	 * 
	 * @param number the number to be checked.
	 * @param message the message of the exception thrown if the number is
	 * lower than or equals to 0.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is lower than 0 or equals
	 * to 0. The message of this exception will be the provided.
	 */
	public static int requirePositive(int number, String message) {
		return check(number, x -> x > 0, message);
	}

	/**
	 * Checks if the provided number is greater than or 0.
	 * 
	 * @param number the number to be checked.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is lower than or equals
	 * to 0.
	 */
	public static double requirePositive(double number) {
		return requirePositive(number, "number must be greater than 0");
	}

	/**
	 * Checks if the provided number is greater than 0.
	 * 
	 * @param number the number to be checked.
	 * @param message the message of the exception thrown if the number is
	 * lower than or equals to 0.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is lower than 0 or equals
	 * to 0. The message of this exception will be the provided.
	 */
	public static double requirePositive(double number, String message) {
		return check(number, x -> x > 0, message);
	}

	/**
	 * Checks if the provided number is finite and greater than or equals to 0.
	 * 
	 * @param number the number to be checked.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is infinite or lower than
	 * 0.
	 */
	public static double requireFiniteNonNegative(double number) {
		return requireNonNegative(requireFinite(number));
	}

	/**
	 * Checks if the provided number is finite and greater than or equals to 0.
	 * 
	 * @param number the number to be checked.
	 * @param message the message of the exception thrown if the number is
	 * infinite or lower than 0.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is infinite or lower than
	 * 0. The message of this exception will be the provided.
	 */
	public static double requireFiniteNonNegative(double number, String message) {
		return requireNonNegative(requireFinite(number, message), message);
	}

	/**
	 * Checks if the provided number is finite and greater than or equals to 0.
	 * 
	 * @param number the number to be checked.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is infinite or lower than
	 * 0.
	 */
	public static long requireNonNegative(long number) {
		return requireNonNegative(number);
	}

	/**
	 * Checks if the provided number is greater than or equals to 0.
	 * 
	 * @param number the number to be checked.
	 * @param message the message of the exception thrown if the number is
	 * lower than 0.
	 * @return the provided number.
	 * @throws IllegalArgumentException if the number is lower than 0. The
	 * message of this exception will be the provided.
	 */
	public static long requireNonNegative(long number, String message) {
		return check(number, x -> x >= 0, message);
	}
	
	private static <T> T check(
		T value, Function<T, Boolean> validator, String message
	) {
		if (validator.apply(value)) return value;
		else throw new IllegalArgumentException(message);
	}
	
	private static double check(
		double value, DoubleFunction<Boolean> validator, String message
	) {
		if (validator.apply(value)) return value;
		else throw new IllegalArgumentException(message);
	}
	
	private static int check(
		int value, DoubleFunction<Boolean> validator, String message
	) {
		if (validator.apply(value)) return value;
		else throw new IllegalArgumentException(message);
	}
	
	private static long check(
		long value, DoubleFunction<Boolean> validator, String message
	) {
		if (validator.apply(value)) return value;
		else throw new IllegalArgumentException(message);
	}
	
	private static <T> Function<T, Boolean> not(Function<T, Boolean> function) {
		return x -> !function.apply(x);
	}
	
	private static DoubleFunction<Boolean> notDouble(DoubleFunction<Boolean> function) {
		return x -> !function.apply(x);
	}
}
