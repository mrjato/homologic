/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.services.entities;

import static es.uvigo.ei.sing.homologic.util.Checks.requirePositive;
import static es.uvigo.ei.sing.homologic.util.Checks.requireStringSize;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import es.uvigo.ei.sing.homologic.persistence.entities.Genome;

@XmlRootElement(name = "genome-metadata", namespace = "http://sing.ei.uvigo.es/homologic")
@XmlAccessorType(XmlAccessType.FIELD)
public class GenomeMetadata {
	@NotNull(message = "Id can not be null")
	@XmlAttribute(required = true)
	private int id;

	@NotNull(message = "Name can not be null")
	@Size(min = 1, max = 1024,
		message = "Name can not be empty or have more than 1024 characters")
	@XmlElement(required = true, nillable = false)
	private String name;

	@NotNull(message = "Number of genes can not be null")
	@Min(value = 1, message = "Number of genes must be greater than 0")
	@XmlElement(required = true, nillable = false)
	private int numGenes;
	
	GenomeMetadata() {}
	
	public GenomeMetadata(Genome genome) {
		this(genome.getId(), genome.getName(), genome.getNumGenes());
	}

	public GenomeMetadata(int id, String name, int numGenes) {
		this.setId(id);
		this.setName(name);
		this.setNumGenes(numGenes);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = requireStringSize(name, 1, 1024);
	}

	public int getNumGenes() {
		return numGenes;
	}

	public void setNumGenes(int numGenes) {
		this.numGenes = requirePositive(numGenes);
	}
}
