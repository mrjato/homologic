/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.services.entities;

import static es.uvigo.ei.sing.homologic.util.Checks.requireNonEmpty;
import static es.uvigo.ei.sing.homologic.util.Checks.requireStringSize;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "gene", namespace = "http://sing.ei.uvigo.es/homologic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Gene {
	@NotNull(message = "Name can not be null")
	@Size(min = 1, max = 1024, message = "Name can not be empty or have more than 1024 characters")
	@XmlElement(nillable = false, required = true)
	private String name;
	
	@NotNull(message = "Sequence can not be null")
	@Size(min = 1, message = "Sequence can not be empty")
	@XmlElement(nillable = false, required =  true)
	private String sequence;
	
	Gene() {
	}
	
	public Gene(String name, String sequence) {
		this.setName(name);
		this.setSequence(sequence);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = requireStringSize(name, 1, 1024,
			"Name can not be empty or have more than 1024 characters");
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = requireNonEmpty(sequence, "Name can not be empty");
	}
}
