/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.services;


import java.util.function.Function;
import java.util.function.Supplier;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import es.uvigo.ei.sing.homologic.controllers.UserController;
import es.uvigo.ei.sing.homologic.persistence.entities.RoleType;
import es.uvigo.ei.sing.homologic.persistence.entities.User;
import es.uvigo.ei.sing.homologic.services.entities.ErrorMessage;
import es.uvigo.ei.sing.homologic.util.ThrowingSupplier;

public final class ServiceUtils {
	private ServiceUtils() {
	}
	
	public static Supplier<Response> createResponseErrorBuilder(
		Response.Status status,
		Exception exception
	) {
		return () -> createErrorResponse(status, exception);
	}
	
	public static Supplier<Response> createResponseErrorBuilder(
		Response.Status status,
		String message
	) {
		return () -> createErrorResponse(status, message);
	}

	public static Response createUnauthorizedResponse(String message) {
		return createErrorResponse(Status.UNAUTHORIZED, message);
	}
	
	public static Response createUnauthorizedResponse(Exception exception) {
		return createErrorResponse(Status.UNAUTHORIZED, exception);
	}

	public static Response createNotFoundResponse(String message) {
		return createErrorResponse(Status.NOT_FOUND, message);
	}
	
	public static Response createNotFoundResponse(Exception exception) {
		return createErrorResponse(Status.NOT_FOUND, exception);
	}

	public static Response createBadRequestResponse(String message) {
		return createErrorResponse(Status.BAD_REQUEST, message);
	}
	
	public static Response createBadRequestResponse(Exception exception) {
		return createErrorResponse(Status.BAD_REQUEST, exception);
	}

	public static Response createInternalServerErrorResponse(String message) {
		return createErrorResponse(Status.INTERNAL_SERVER_ERROR, message);
	}
	
	public static Response createInternalServerErrorResponse(Exception exception) {
		return createErrorResponse(Status.INTERNAL_SERVER_ERROR, exception);
	}
	
	public static Response createErrorResponse(
		Response.Status status,
		Exception exception
	) {
		return createErrorResponse(status, exception.getMessage());
	}

	public static Response createErrorResponse(
		Response.Status status,
		String message
	) {
		return Response.status(status)
			.entity(new ErrorMessage(
				status.getStatusCode(),
				message
			))
		.build();
	}
	
	public static String getUserName(SecurityContext security) {
		return security.getUserPrincipal().getName();
	}
	
	public static User getUser(SecurityContext security, UserController controller) {
		return controller.get(getUserName(security));
	}
	
	public static Response doIfPrivileged(
		SecurityContext security,
		String user,
		ThrowingSupplier<Response> actionOk
	) {
		return doIfPrivileged(
			security,
			user,
			actionOk,
			e -> ServiceUtils.createErrorResponse(
				Status.INTERNAL_SERVER_ERROR, e
			),
			() -> ServiceUtils.createErrorResponse(
				Status.UNAUTHORIZED, "Unauthorized access"
			)
		);
	}
	
	public static Response doIfPrivileged(
		SecurityContext security,
		String user,
		ThrowingSupplier<Response> actionOk,
		Function<Exception, Response> actionError,
		ThrowingSupplier<Response> accessError
	) {
		try {
			if (security.isUserInRole(RoleType.ADMIN.id()) ||
				user.equals(ServiceUtils.getUserName(security))
			) {
				return actionOk.get();
			} else {
				return accessError.get();
			}
		} catch (Exception e) {
			return actionError.apply(e);
		}
	}
}
