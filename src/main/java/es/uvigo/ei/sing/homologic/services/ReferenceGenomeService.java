/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.services;

import static es.uvigo.ei.sing.homologic.services.ServiceUtils.createBadRequestResponse;
import static es.uvigo.ei.sing.homologic.services.ServiceUtils.createInternalServerErrorResponse;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.sun.jersey.multipart.FormDataParam;

import es.uvigo.ei.sing.homologic.controllers.GenomeController;
import es.uvigo.ei.sing.homologic.fasta.FastaParseException;
import es.uvigo.ei.sing.homologic.persistence.entities.ReferenceGenome;
import es.uvigo.ei.sing.homologic.persistence.file.StorageException;
import es.uvigo.ei.sing.homologic.services.entities.GenomeCreationMetadata;
import es.uvigo.ei.sing.homologic.services.entities.GenomeMetadata;
import es.uvigo.ei.sing.homologic.services.entities.Message;
import es.uvigo.ei.sing.homologic.services.entities.UUID;

/**
 * Service for managing the reference genomes.
 * 
 * @author Miguel Reboiro-Jato
 */
@Path("genome/reference")
@Service
@RolesAllowed("hl-admin")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class ReferenceGenomeService {
	private final static Logger LOG = LoggerFactory.getLogger(ReferenceGenomeService.class);
	
	@Inject
	private GenomeController genomeController;
	
	@Context
	private SecurityContext security;
	
	/**
	 * <p>
	 * Uploads a new reference genome file. The file will be just uploaded and
	 * stored as a raw file, and no data will be stored in the database.
	 * </p>
	 * <p>
	 * When a genome file is correctly uploaded, a UUID will be returned that
	 * can be used in the {@code #storeReference(GenomeMetadata)} method to
	 * create a new reference genome for the corresponding user.
	 * </p>
	 * <p>
	 * <strong>Important: </strong>the content type of the request must be
	 * "multipart/form-data".
	 * </p>
	 * <p>This method can be invoked only by admin users.</p>
	 * 
	 * @param is The content of the request.
	 * @return The generated UUID with a 200 OK HTTP status if registration was
	 * successful or an error message with a 500 Internal Server Error HTTP
	 * status if an error occurs.
	 */
	@POST
	@Path("/file")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response upload(
		@FormDataParam("genome") InputStream is
	) {
		try {
			final String uuid = genomeController.storeReference(is);
			
			return Response.ok(
				new UUID(uuid)
			).build();
		} catch (Exception ioe) {
			LOG.error("Error uploading reference genome", ioe);
			
			return createInternalServerErrorResponse(
				"File could not be stored"
			);
		}
	}
	
	/**
	 * Creates a new reference genome for the invoking user. The user must
	 * provide a valid UUID for a genome file previously uploaded.
	 * 
	 * <p>This method can be invoked only by admin users.</p>
	 * 
	 * @param genomeMetadata The name of the reference genome and the UUID of
	 * its file.
	 * @return The metadata of the new genome with a 200 OK HTTP status if
	 * registration was successful, an error message with a 400 Bad Request
	 * HTTP status if the genome is not valid or an error message with a 500
	 * Internal Server Error HTTP status if an error occurs.
	 */
	@POST
	public Response store(
		GenomeCreationMetadata genomeMetadata
	) {
		try {
			final ReferenceGenome genome = genomeController.createReference(
				genomeMetadata.getName(), genomeMetadata.getUuid()
			);
			return Response.ok(
				new GenomeMetadata(genome)
			).build();
		} catch (StorageException se) {
			LOG.error("Error storing reference genome", se);
			
			return createInternalServerErrorResponse(
				"Error storing file: " + se.getMessage()
			);
		} catch (IOException | FastaParseException e) {
			LOG.error("Error parsing fasta file", e);
			
			return createBadRequestResponse(
				"Error parsing fasta file: " + e.getMessage()
			);
		} catch (Exception e) {
			LOG.error("Error storing genome", e);
			
			return createInternalServerErrorResponse(
				"Error storing genome file: " + e.getMessage()
			);
		}
	}
	
	/**
	 * Returns the basic data of the reference genomes in the system. The data
	 * is composed of the id, name and number of genes of the genome.
	 * 
	 * @return The basic data of the reference genomes in the system with a 200
	 * OK HTTP status if registration was successful or an error message with a
	 * 500 Internal Server Error HTTP status if an error occurs.
	 */
	@GET
	@RolesAllowed({ "hl-admin", "hl-user" })
	@Consumes(MediaType.WILDCARD)
	public Response list() {
		final List<ReferenceGenome> referenceGenomes =
			genomeController.listReference();
		
		final List<GenomeMetadata> genomes = referenceGenomes.stream()
			.map(GenomeMetadata::new)
		.collect(toList());
		
		return Response.ok(genomes).build();
	}
	
	/**
	 * Deletes a reference genome from the system. 
	 * 
	 * <p>This method can be invoked only by admin users.</p>
	 * 
	 * @return A confirmation message with a 200 OK HTTP status if deletion was
	 * successful, an error message with a 400 Bad Request HTTP status if the
	 * user does not have a test genome with the provided id or an error
	 * message with a 500 Internal Server Error HTTP status if an error occurs.
	 */
	@DELETE
	@Path("/{genomeId}")
	@Consumes(MediaType.WILDCARD)
	public Response delete(
		@PathParam("genomeId") int genomeId
	) {
		final ReferenceGenome test = genomeController.getReference(genomeId);
		
		if (test == null) {
			return createBadRequestResponse("No genome found.");
		} else {
			try {
				genomeController.deleteReference(genomeId);
	
				return Response.ok(
					new Message("Reference genome " + genomeId + " deleted")
				).build();
			} catch (Exception e) {
				LOG.error("Error deleting reference genome " + genomeId, e);
				
				return createInternalServerErrorResponse(
					"Error deleting reference genome: " + e.getMessage()
				);
			}
		}
	}
}
