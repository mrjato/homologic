/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.controllers;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Controller;

import es.uvigo.ei.sing.homologic.persistence.daos.RegistrationDAO;
import es.uvigo.ei.sing.homologic.persistence.daos.UserDAO;
import es.uvigo.ei.sing.homologic.persistence.entities.Registration;
import es.uvigo.ei.sing.homologic.persistence.entities.User;

@Controller
@Transactional
public class RegistrationController {
	@Inject
	private RegistrationDAO registrationDAO;
	
	@Inject
	private UserDAO userDAO;
	
	public Registration register(String login, String email, String password) {
		checkIfUserAlreadyExists(login, email);
		
		deleteRegistrationWithEmail(email);
		
		return registrationDAO.persist(new Registration(login, email, password));
	}
	
	public User confirm(String uuid) {
		final Registration registration = registrationDAO.get(uuid);
		
		if (registration == null) {
			throw new IllegalArgumentException("Invalid UUID");
		} else {
			registrationDAO.remove(registration);
			
			return userDAO.registerNormalUser(registration.toUser());
		}
	}

	protected void checkIfUserAlreadyExists(String login, String email) {
		if (userDAO.get(login) != null)
			throw new IllegalArgumentException("Login already exists");
		if (userDAO.getByEmail(email) != null)
			throw new IllegalArgumentException("Email already exists");
	}
	
	protected void deleteRegistrationWithEmail(String email) {
		final Registration registration = this.registrationDAO.getByEmail(email);
		
		if (registration != null) {
			registrationDAO.remove(registration);
		}
	}
}
