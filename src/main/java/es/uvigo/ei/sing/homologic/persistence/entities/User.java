/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.persistence.entities;

import static es.uvigo.ei.sing.homologic.util.Checks.EMAIL_PATTERN;
import static es.uvigo.ei.sing.homologic.util.Checks.MD5_PATTERN;
import static es.uvigo.ei.sing.homologic.util.Checks.requireEmail;
import static es.uvigo.ei.sing.homologic.util.Checks.requireMD5;
import static es.uvigo.ei.sing.homologic.util.Checks.requireStringSize;
import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 50)
	@NotNull(message = "Login can not be null")
	@Size(min = 1, max = 50, message = "Login must have between 1 and 50 characters")
	private String login;
	
	@Column(unique = true, length = 100)
	@NotNull
	@Pattern(regexp = EMAIL_PATTERN, message = "Invalid email address")
	@Size(min = 5, max = 100, message = "Invalid email address")
	private String email;
	
	@Column(unique = true, length = 32)
	@NotNull(message = "Password can not be null")
	@Size(min = 32, max = 32, message = "Password must be MD5 digested")
	@Pattern(regexp = MD5_PATTERN, message = "Password must be MD5 digested")
	private String password;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<TestGenome> genomes;
	
	User() {
	}
	
	public User(String login, String email, String password) {
		this.login = login;
		this.email = email;
		this.password = password;
		this.genomes = new HashSet<>();
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = requireStringSize(login, 1, 50, "Login must have between 1 and 50 characters");
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = requireEmail(email, "Invalid email address");
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = requireMD5(password, "Invalid password");
	}

	public Set<TestGenome> getGenomes() {
		return Collections.unmodifiableSet(this.genomes);
	}

	public boolean addGenome(TestGenome genome) {
		requireNonNull(genome, "genome can't be null");

		if (!this.genomes.contains(genome)) {
			genome.setUser(this);

			return true;
		} else {
			return false;
		}
	}

	public boolean removeGenome(TestGenome genome) {
		requireNonNull(genome, "genome can't be null");

		if (this.equals(genome.getUser())) {
			genome.setUser(null);

			return true;
		} else {
			return false;
		}
	}
	
	public boolean containsGenome(TestGenome genome) {
		return this.genomes.contains(genome);
	}

	void _addGenome(TestGenome genome) {
		this.genomes.add(genome);
	}

	void _removeGenome(TestGenome genome) {
		this.genomes.remove(genome);
	}

	
	@Override
	public String toString() {
		return this.login + " - " + this.password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		User other = (User) obj;
		if (login == null) {
			if (other.login != null) {
				return false;
			}
		} else if (!login.equals(other.login)) {
			return false;
		}
		if (password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		return true;
	}
}
