/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.util;


import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.Resource;
import javax.inject.Singleton;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.stereotype.Component;

@Singleton
@Resource
@Component
public final class Configuration {
	private static final String TEST_GENOMES_DIRECTORY_NAME = "test_genomes";
	private static final String REFERENCE_GENOMES_DIRECTORY_NAME = "reference_genomes";
	
	private volatile InitialContext initialContext;
	
	public Configuration() {}
	
	private synchronized void createInitialContext() throws NamingException {
		if (this.initialContext == null)
			this.initialContext = new InitialContext();
	}
	
	private InitialContext getInitialContext() throws NamingException {
		if (this.initialContext == null ){
			this.createInitialContext();
		}
		
		return this.initialContext;
	}
	
	@SuppressWarnings("unchecked")
	private <T> T getConfigParam(String param){
		param = "homologic." + param;
		try {
			return (T) this.getInitialContext().lookup("java:comp/env/" + param);
		} catch (NamingException e) {
			return null;
		}
	}
	
	public File getDataDirectory() {
		return new File((String) this.getConfigParam("storage.files.dataset.dir"));
	}
	
	public Path getDataDirectoryPath() {
		return Paths.get((String) this.getConfigParam("storage.files.dataset.dir"));
	}
	
	public File getTestGenomeDirectory() {
		return new File(this.getDataDirectory(), Configuration.TEST_GENOMES_DIRECTORY_NAME);
	}
	
	public Path getTestGenomeDirectoryPath() {
		return this.getDataDirectoryPath().resolve(Configuration.TEST_GENOMES_DIRECTORY_NAME);
	}
	
	public File getReferenceGenomeDirectory() {
		return new File(this.getDataDirectory(), Configuration.REFERENCE_GENOMES_DIRECTORY_NAME);
	}
	
	public Path getReferenceGenomeDirectoryPath() {
		return this.getDataDirectoryPath().resolve(Configuration.REFERENCE_GENOMES_DIRECTORY_NAME);
	}
}
