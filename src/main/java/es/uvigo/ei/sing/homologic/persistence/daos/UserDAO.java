/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.persistence.daos;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import es.uvigo.ei.sing.homologic.persistence.entities.Role;
import es.uvigo.ei.sing.homologic.persistence.entities.RoleType;
import es.uvigo.ei.sing.homologic.persistence.entities.User;

@Repository
@Transactional
public class UserDAO extends DAO<String, User> {
	public User getByEmail(String email) {
		return getBy("email", email);
	}
	
	public User registerNormalUser(User user) {
		persist(user);
		em.persist(new Role(user, RoleType.USER));
		
		return user;
	}
	
	public User registerAdminUser(User user) {
		persist(user);
		em.persist(new Role(user, RoleType.ADMIN));
		
		return user;
	}
}
