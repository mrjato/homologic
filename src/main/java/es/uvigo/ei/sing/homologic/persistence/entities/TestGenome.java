/*
 *  This file is part of HomoLogic.
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.ei.sing.homologic.persistence.entities;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class TestGenome extends Genome {
	@ManyToOne(fetch = FetchType.LAZY)
	private User user;
	
	@OneToMany(
		fetch = FetchType.LAZY,
		orphanRemoval = true,
		cascade = CascadeType.ALL
	)
    private List<TestGene> genes;
	
	TestGenome() {
	}
	
	public TestGenome(
		String name, String path, User user, Collection<TestGene> genes
	) {
		super();
		
		this.numGenes = 0;
		this.setName(name);
		this.setUuid(path);
		this.setUser(user);
		this.genes = new LinkedList<>();
		genes.stream().forEach(this::addGene);
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		if (this.user != null) {
			this.user._removeGenome(this);
		}

		this.user = user;

		if (this.user != null) {
			this.user._addGenome(this);
		}
	}
	
	public List<TestGene> getGenes() {
		return Collections.unmodifiableList(this.genes);
	}

	public boolean addGene(TestGene gene) {
		Objects.requireNonNull(gene, "gene can't be null");

		if (!this.genes.contains(gene)) {
			gene.setGenome(this);
			
			return true;
		} else {
			return false;
		}
	}

	public boolean removeGene(TestGene gene) {
		Objects.requireNonNull(gene, "gene can't be null");

		if (this.equals(gene.getGenome())) {
			gene.setGenome(null);

			return true;
		} else {
			return false;
		}
	}

	void _addGene(TestGene gene) {
		this.genes.add(gene);
//		Collections.sort(this.genes);

		super.setNumGenes(genes.size());
	}

	void _removeGene(TestGene gene) {
		this.genes.remove(gene);

		super.setNumGenes(genes.size());
	}
}
